/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class apxt_restTypes {
    global apxt_restTypes() {

    }
global class FrontendRecipient extends APXT_CongaSign.apxt_restTypes.TransactionRecipient {
    global FrontendRecipient() {

    }
}
global class PostResponse {
}
global class TransactionParams {
    global TransactionParams() {

    }
}
global virtual class TransactionRecipient {
    global TransactionRecipient() {

    }
}
global class WebappResponse {
    global WebappResponse() {

    }
}
global class WebappResponseDetails {
    global WebappResponseDetails() {

    }
}
}
