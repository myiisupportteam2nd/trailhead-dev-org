<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X1_hour_warning_before_milestone_expires</fullName>
        <description>1 hour warning before milestone expires.</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SLA_1_Hour_Warning_Before_Milestone_Expires</template>
    </alerts>
</Workflow>
