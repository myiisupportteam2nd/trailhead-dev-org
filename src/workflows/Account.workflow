<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email_Template</fullName>
        <description>Approval Email Template</description>
        <protected>false</protected>
        <recipients>
            <recipient>d.siwapongsakorn+trailhead@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_Email_Template</template>
    </alerts>
</Workflow>
