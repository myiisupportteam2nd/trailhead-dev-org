global class BatchExportReportCSVController implements Database.Batchable<sObject>
{
	//Retrieve ORG Wide Default Email Address ID
	//private static final Id ORG_EMAIL_ADDRESS_ID = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'no-reply@tafesa.edu.au' Limit 1][0].Id;
	
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        // collect the batches of records or objects to be passed to execute
        return Database.getQueryLocator(' SELECT Id, Name, Capacity_Max__c, Disable_Booking__c, StartDate, Status, Type, Remaining_Capacity__c, IsActive'+
                                        ' FROM Campaign'+
                                        ' WHERE LastModifiedDate = TODAY');
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject> sobjs)
    {
        ExportReportCSVController.exportReportAsCSVFile();
    }
        
    global void finish(Database.BatchableContext BC)
    {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
		//Below code will fetch the job Id
        AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];
                
        //below code will send an email to User about the status
        String email = 'd.siwapongsakorn@gmail.com';
        mail.setToAddresses(new List<String>{email});
        
        //Specific ORG Wide Default Email
        //mail.setOrgWideEmailAddressId(ORG_EMAIL_ADDRESS_ID);
        mail.setReplyTo(email);//Add here your email address
        mail.setSubject('Export Report into CSV - Batch Update Event Start Date Processing '+a.Status);
        mail.setPlainTextBody('The Batch Apex job processed  '+a.TotalJobItems+' batches with  '+a.NumberOfErrors+' failures'+' Job Item processed are'+a.JobItemsProcessed);
        Messaging.sendEmail(new Messaging.Singleemailmessage [] {mail});
    }
}