public class AccountDataEncrytionController 
{
    private final Account acct;
    private static Id accountID;
    public String mAccountEmail {get; set;}
    private static Id SYSTEM_ADMIN_USER = [SELECT Id, Name FROM Profile WHERE Name = 'Standard User' LIMIT 1][0].Id;
    
	public AccountDataEncrytionController(ApexPages.StandardController accSTDController)
    {
        this.acct = (Account)accSTDController.getRecord();
        
        accountID = apexpages.currentpage().getparameters().get('id');
        
        mAccountEmail = [SELECT Id, Email__c FROM Account WHERE Id =: accountID LIMIT 1][0].Email__c;
        
        if(UserInfo.getProfileId() != SYSTEM_ADMIN_USER)
        {
            mAccountEmail = '**********';
        }

        //Added Debug Value
        System.debug('>>>>>> mAccountEmail: '+mAccountEmail);
    }

}