public class ImportExcelToAccountController 
{
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Account> mAccsToUpload;
    
	public PageReference fileAccess() {
        return null;
    }

    public ImportExcelToAccountController()
    {
    
    }
    
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        mAccsToUpload = new List<Account>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Account mAccountUpload = new Account();
            mAccountUpload.Name = inputvalues[0];
            mAccountUpload.Type = inputvalues[1];
            mAccountUpload.BillingStreet = inputvalues[2];       
            mAccountUpload.BillingCity = inputvalues[3];
            mAccountUpload.BillingState = inputvalues[4];
            mAccountUpload.BillingPostalCode = inputvalues[5];
            mAccountUpload.BillingCountry = inputvalues[6];
            mAccountUpload.Phone = inputvalues[7];
            mAccountUpload.Fax = inputvalues[8];

            mAccsToUpload.add(mAccountUpload);
        }
        
        try
        {
        	insert mAccsToUpload;
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template or try again later');
            ApexPages.addMessage(errormsg);
        }    
        return null;
    }
    
    public List<Account> getuploadedAccounts()
    {
        if (mAccsToUpload!= NULL)
        {
            if (mAccsToUpload.size() > 0)
            {
                return mAccsToUpload;
            }
            else
            {
                return null;
            }
        }                       
        else
        {
            return null;
        }
    }
}