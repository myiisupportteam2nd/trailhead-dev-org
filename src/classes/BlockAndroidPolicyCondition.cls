global class BlockAndroidPolicyCondition implements TxnSecurity.PolicyCondition {

    public boolean evaluate(TxnSecurity.Event e) 
    {
        LoginHistory eObj = [SELECT Platform FROM LoginHistory WHERE Id = :e.data.get('LoginHistoryId')];
        
        if(eObj != null)
        {
            if(eObj.Platform.contains('Android') && eObj.Platform.compareTo('Android 5') < 0) // Changed
            {
                return true;
            }
        }
        
        
        return false; 
    }
}