public class ExportReportCSVController 
{
  	
  	public static void exportReportAsCSVFile()
  	{
  		try
  		{
  			/**************************************
  			* Query with the specific logic
  			**************************************/
  			
  			List<Account> mAllAccountList = [	SELECT Id, Name , CreatedDate , LastModifiedDate 
  												FROM Account 
  												LIMIT 100];
  												
			string mCSVheader = 'Record Id, Name , Created Date, Modified Date \n';
			string mCSVFinalstr = mCSVheader;
			for(Account mAccountItem: mAllAccountList)
			{
			       String mRecordString = '"'+mAccountItem.id+'","'+mAccountItem.Name+'","'+mAccountItem.CreatedDate+'","'+mAccountItem.LastModifiedDate +'"\n';
			       mCSVFinalstr = mCSVFinalstr +mRecordString;
			}
  			
  			/**************************************
  			* Sending Email with Attachment Process
  			**************************************/
  			
  			//Convert CSV file to Blob and Attach CSV file
			Messaging.EmailFileAttachment mEmailWithAttachment = new Messaging.EmailFileAttachment();
			Blob mCSVBlob = Blob.valueOf(mCSVFinalstr);
			
			String mCSVName= 'All Account - '+Date.TODAY();
			mEmailWithAttachment.setFileName(mCSVName);
			mEmailWithAttachment.setBody(mCSVBlob);
			
			//Send an email with Email
			Messaging.SingleEmailMessage mEmailAlert =new Messaging.SingleEmailMessage();
			String[] toAddresses = new list<string> {'d.siwapongsakorn@gmail.com'};
			String mSubject ='Export All Account Record - '+Date.TODAY();
			mEmailAlert.setSubject(mSubject);
			mEmailAlert.setToAddresses( toAddresses );
			mEmailAlert.setPlainTextBody('Account CSV ');
			mEmailAlert.setFileAttachments(new Messaging.EmailFileAttachment[]{mEmailWithAttachment});
			Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mEmailAlert});
  		}
  		catch(System.Exception ex)
  		{
  			System.debug('>>>>> Error message from ExportReportCSVController: '+ex.getMessage()+' at Line: '+ex.getLineNumber());
  		}
  	}
}